Codes = {
    SUCCESS: {value: 0, name: "Success"},
    FAIL: {value: 1, name: "Failed"},
    INVALID_TOKEN: {value: 2, name: "Invalid Token"},
    DISCONNECTED: {value: 3, name: "Disconnected"},
    NO_LICENSE: {value: 4, name: "No License"},
    UNAUTHORIZED: {value: 5, name: "Unauthorized"},
    NOT_FOUND: {value: 6, name: "Not Found"},
    UNSUPPORTED_CLIENT: {value: 7, name: "Unsupported Client"},
    INTERNAL: {value: 8, name: "Internal"},
    INVALID_LOGIN: {value: 9, name: "Invalid Login"}
};

module.exports = Codes;