var Utils = require('../Utils/Utils');
var JsonCode = require('../Utils/JsonCode');

var m_connection;

/*
 * Requests
 */
var onRequestDataCompleted = function (jsonResults) {
    Utils.ResponseSuccess(m_connection, jsonResults);
};

var onRequestDataFailed = function (strError, code) {
    Utils.ResponseError(m_connection, strError, code);
};


/*
 * @void
 * @return onRequestDataFailed (strError, code)
 */
exports.GET = function (connection) {
    // Error
    m_connection = connection;
    onRequestDataFailed("Service not found", JsonCode.NOT_FOUND.value);
};

/*
 * @void
 * @return onRequestDataCompleted (json)
 * @return onRequestDataFailed (strError, code)
 */
exports.POST = function (connection) {
    console.log("Login -", (new Date()).toLocaleString());
    /*
     URL: /api/login
     */
    m_connection = connection;
    var payload = connection.payload;
    var encrypt = payload['encrypt'] ? payload['encrypt'] : "";

    if (encrypt && encrypt != "") {
        // Check encrypt
        onRequestDataCompleted(responseJson);
    }
    else {
        // Error
        onRequestDataFailed("Service not found", JsonCode.NOT_FOUND.value);
    }
};

/*
 * @void
 * @return onRequestDataFailed (strError, code)
 */
exports.PUT = function (connection) {
    // Error
    m_connection = connection;
    onRequestDataFailed("Service not found", JsonCode.NOT_FOUND.value);
};

/*
 * @void
 * @return onRequestDataFailed (strError, code)
 */
exports.DELETE = function (connection) {
    // Error
    m_connection = connection;
    onRequestDataFailed("Service not found", JsonCode.NOT_FOUND.value);
};
