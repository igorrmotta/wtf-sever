var Config = require('../Config');
var JsonCode = require('../Utils/JsonCode');

var db = require('../MySQLDB');
db.CreateConnection();
var m_Database = db.GetDb();

var m_tableUser = 'User';

/*
 * @void
 * @param {int} idUser
 * @param {int} type
 * @return onRequestDataCompleted (userJson)
 * @return onRequestDataFailed (strError, code)
 */
exports.GetUser = function (idUser, type, onRequestDataCompleted, onRequestDataFailed)
{
    if (!type || type == "")
    {
        type = 0; // Sem tipo;
    }

    var sqlUser = "CALL getUser(" + m_Database.escape(idUser) + "," + type + ");";
    console.log("  SQL - GetUser: \n   ", sqlUser);

    m_Database.query(sqlUser, function (error, results)
    {
        if (error)
        {
            onRequestDataFailed(error, JsonCode.INTERNAL.value);
        }
        else
        {
            onRequestDataCompleted(results[0]);
        }
    });
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.AddUser = function (user, onRequestDataCompleted, onRequestDataFailed)
{
    var sqlUser = "INSERT INTO " +
        m_tableUser +
        " SET ?;";

    console.log("  SQL - AddUser: \n   ", sqlUser);

    m_Database.query(sqlUser, user, function (errorUser, result)
    {
        if (errorUser)
        {
            onRequestDataFailed(errorUser, JsonCode.INTERNAL.value);
        }
        else
        {
            console.log("Add user: ", result);
            onRequestDataCompleted({idUser: result.insertId});
        }
    });
};
