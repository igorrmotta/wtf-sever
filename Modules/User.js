/**
 * Created by Enrique on 24/08/14.
 */

/* Connection
 connection.httpRequest = request;
 connection.httpResponse = response;
 connection.httpMethod = request.method.toUpperCase();
 connection.payload = data;
 connection.isUserAuthenticated
 */

var Database = require('./UsersDb');
var Utils = require('../Utils/Utils');

var JsonCode = require('../Utils/JsonCode');

var m_connection;

/*
 * Requests
 */
var onRequestDataCompleted = function (jsonResults)
{
    Utils.ResponseSuccess(m_connection, jsonResults);
};

var onRequestDataFailed = function (strError, code)
{
    Utils.ResponseError(m_connection, strError, code);
};


/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.GET = function (connection)
{
    console.log("User GET -", (new Date()).toLocaleString());

    m_connection = connection;
    var payload = connection.payload;
    var idUser = payload['id'];

    /*
     URL: /api/user?id=111111
     */
    if (idUser)
    {
        Database.GetUser(idUser, "", onRequestDataCompleted, onRequestDataFailed);
    }
    else
    {
        // Error
        onRequestDataFailed("Service not found", JsonCode.NOT_FOUND.value);
    }
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.POST = function (connection)
{
    console.log("User POST -", (new Date()).toLocaleString());

    /*
     URL: /api/user
     */
    m_connection = connection;
    var payload = connection.payload;

    var user = Utils.CreateUser();
    user.name = payload['name'];
    user.encrypt = payload['encrypt'];
    user.email = payload['email'];
    user.active = Utils.IsActive;

    // Optional
    user.city = payload['city'];
    user.state = payload['state'];
    user.country = payload['country'];
    user.street = payload['street'];
    user.district = payload['district'];
    user.cep = payload['cep'];
    user.birthDate = payload['birthDate'];
    user.telephone = payload['telephone'];

    if (user.encrypt && user.encrypt != "")
    {
        Database.AddUser(user, onRequestDataCompleted, onRequestDataFailed);
    }
    else
    {
        // Error
        onRequestDataFailed("Service not found", JsonCode.NOT_FOUND.value);
    }
};

exports.PUT = function (connection)
{
    console.log("User PUT -", (new Date()).toLocaleString());

    /*
     URL: /api/user
     */
    m_connection = connection;


    // Error
    onRequestDataFailed("Not implemented.", JsonCode.INTERNAL.value);
};

exports.DELETE = function (connection)
{
    console.log("User DELETE -", (new Date()).toLocaleString());

    /*
     URL: /api/user
     */
    m_connection = connection;


    // Error
    onRequestDataFailed("Not implemented.", JsonCode.INTERNAL.value);
};