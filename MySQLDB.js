var Db = require('mysql');
Config = require('./Config');

var m_dbConnection;
exports.CreateConnection = function ()
{
    m_dbConnection = Db.createConnection({
        host: Config.dbHost,
        port: Config.dbPort,
        user: Config.dbUser,
        password: Config.dbPassword,
        database: Config.dbDatabase
    });
};

exports.GetDb = function ()
{
    return m_dbConnection;
};

exports.CloseDb = function ()
{
    m_dbConnection.end();
};


