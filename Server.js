var Http = require('http'),
    URL = require('url'),
    Utils = require('./Utils/Utils'),
    JsonCode = require('./Utils/JsonCode');

// Módulos
var Login = require('./Features/Login');
var Test = require('./Dev/Test');
var User = require('./Modules/User');

/*
 SERVER
 */
var server = Http.createServer(function (request, response)
{
    if (request.url == '/favicon.ico')
    {
        response.end();
        return;
    }

    var connection = {};
    connection.httpRequest = request;
    connection.httpResponse = response;
    connection.httpMethod = request.method.toUpperCase();
    connection.payload = {};

    if ((new RegExp('^/api/login')).test(request.url))
    {
        Utils.CallIfExists(Login, connection);
        return;
    }

    /*
     ** Route : ˆ/api/*
     */
    if ((new RegExp('^/api/.*')).test(request.url))
    {
        var pathname = URL.parse(request.url, true)['pathname'];
        var service = pathname.substring('/api/'.length);
        var obj;

        switch (service)
        {

            //Features
            case 'user':
            {
                obj = User;
                break;
            }

            case 'login':
            {
                obj = Login;
                break;
            }

            default:
            {
                response.writeHead(404, {'Content-type': 'application/json'});
                response.end(JSON.stringify({message: 'Service not found'}));
                break;
            }
        }
        Utils.CallIfExists(obj, connection);
    }
    /*
     ** Route : ˆ/dev/*
     */
    else if ((new RegExp('^/dev/.*')).test(request.url))
    {
        var pathname = URL.parse(request.url, true)['pathname'];
        var service = pathname.substring('/dev/'.length);
        var obj;

        switch (service)
        {
            case 'test':
            {
                obj = Test;
                break;
            }
            default:
            {
                response.writeHead(404, {'Content-type': 'application/json'});
                response.end(JSON.stringify({message: 'Service not found'}));
                break;
            }
        }
        Utils.CallIfExists(obj, connection);
    }
    else
    {
        response.writeHead(404, {'Content-type': 'application/json'});
        response.end(JSON.stringify({message: 'Service not found'}));
    }
});

server.listen(8080);